#!/usr/bin/env ruby
thread = [1,2,3,4,5,6,7,8,16,32]
number = 1000.step(by: 1000).take(100)
nameout = ARGV[1]
string = File.read(ARGV[0])
supera = Array.new()
format = "%-8s\t%-15s\t%-2s\n"

def median(array)
    sorted = array.sort
    len = sorted.length
    return sorted[1]
end

for i in 1..3
    # This is a fix for only specific datasets that do not include the input value
    # ONLY USE THIS IF YOU KNOW WHAT YOU ARE DOING!
    #supera << string.scan(/.*?(\d+)(?>\s\-|\,).*#{i}:\n([+-]?\d*\.\d+)(?![-+0-9\.]).*?(\d+).*?(\d+)/i)
    # USE THIS ONE INSTEAD FOR YOUR GENERAL USE :-D
    #supera << string.scan(/.*?#{i}:\n([+-]?\d*\.\d+)(?![-+0-9\.]).*?(\d+).*?(\d+)/)
    supera << string.scan(/.*?#{i}:\n.*?(\d+\.?\d+)\s+(\d+)\s+(\d+)\s+(\d+)/)
end

#print supera

for i in 0..supera[0].length-1
    a = Array.new()
    a << supera[0][i][0].to_f
    a << supera[1][i][0].to_f
    a << supera[2][i][0].to_f
    supera[0][i][0] = median(a).to_s
end

#print supera[0]
#print number
for i in thread
    File.open("#{ARGV[1]}.t#{i}", "w+") do |f|
        f.printf(format, "INPUT", "TIME", "THREAD")
        supera[0].each{|x|
            if(x[3].to_i == i)
 #               printf(format, x[0], x[1], x[3])
                f.printf(format, x[1], x[0], x[3])
            end
        }
        # LaTeX PGFPlot formating
        #supera[0].each{|x| print "(#{x[0]}, #{x[1]})\n" if(x[3] == ARGV[1]) }
    end
end
