#!/bin/bash
###############################################################################
# For Coursework 2 F21DP
###############################################################################
# PGH.sh will execute all presented compiled Haskell files with iterations in
# order to generate the profiling information.
#
# Created by Hans-Nikolai Viessmann
###############################################################################
if [ ! -n "${1}" ]; then
    echo "Missing main argument" >&2
    echo "Usage: ${0} BINARY [\"INPUTS\" [THREADS]]" >&2
    exit 1
fi
APP=${1}
OPTS=${2:-"15000 30000 100000"}
THRS=${3:-"1 2 3 4 5 6 7 8 16 32"}
TIMS=${4:-3}
DIR="${PWD}/${APP}-prof"
RUN="${DIR}/${APP}.result"
ERR="${DIR}/${APP}.err"

[ -d "${DIR}" ] || mkdir ${DIR}
if [ ! -x "${APP}" ]; then
    echo "Please compile ${APP}"
    exit 100
fi

echo "Runtime and Profiling for ${APP} by [${OPTS}]" | tee "${RUN}"
echo "This could take a very long time, have a coffee or watch a movie..."
for i in ${OPTS}; do
    for j in ${THRS}; do
        for x in $(seq 1 ${TIMS}); do 
            echo "Range ${i}, thread ${j}, iter ${x}:" >> "${RUN}"
            echo -n "Range ${i}, thread ${j}, iter ${x}: "
            ./"${APP}" 1 ${i} +RTS -N${j} -ls >> "${RUN}"
            echo "DONE"
        done
    done
done
