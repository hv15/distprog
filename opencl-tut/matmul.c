#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <float.h>

#include <CL/cl.h>
#include "simple.h"

#define DATA_SIZE 1024

const char *KernelSource =                 "\n"
  "__kernel void matmul(                    \n"
  "   __global float* matrix_a,             \n"
  "   __global float* matrix_b,             \n"
  "   __global float* output,               \n"
  "   const unsigned int ca)                \n"
  "{                                        \n"
  "  int x = get_global_id(0);              \n"
  "  int y = get_global_id(1);              \n"
  "  for(int i = 0; i < ca; i++){           \n"
  "     output[y * ca + x] += matrix_a[y * ca + i] * matrix_b[i * ca + x]; \n"
  "  }                                      \n"
  "}                                        \n"
  "\n";


struct timespec start, stop;

void printTimeElapsed( char *text)
{
  double elapsed = (stop.tv_sec -start.tv_sec)*1000.0
                  + (double)(stop.tv_nsec -start.tv_nsec)/1000000.0;
  printf( "%s: %f msec\n", text, elapsed);
}

void timeDirectImplementation( int count, float* in_a, float* in_b, float *out)
{
  float sum;

  clock_gettime( CLOCK_PROCESS_CPUTIME_ID, &start);
  for (int i = 0; i < count; i++) {
    for (int j = 0; j < count; j++) {
      sum = 0.0;
      for (int k = 0; k < count; k++) {
        sum += in_a[i*count+k] * in_b[k*count+j];
      }
      out[i*count+j] =sum;
    }
  }
  clock_gettime( CLOCK_PROCESS_CPUTIME_ID, &stop);
  printTimeElapsed( "kernel equivalent on host");
}


int main (int argc, char * argv[])
{
  cl_int err;
  cl_kernel kernel;

// define and set work set here:

  size_t global[2];
  size_t local[2];

  if( argc < 3) {
    local[0] = 32;
	local[1] = 32;
  } else {
    local[0] = atoi(argv[1]);
	local[1] = atoi(argv[2]);
  }

  //printf( "work group size: %d:%d\n", (int) local[0], (int) local[1]);

  clock_gettime( CLOCK_PROCESS_CPUTIME_ID, &start);

  /* Create data for the run.  */
  float *in_a = NULL;                /* Original data set given to device.  */
  float *in_b = NULL;                /* Original data set given to device.  */
  float *out = NULL;             /* Results returned from device.  */
  int correct;                       /* Number of correct results returned.  */

  int count = DATA_SIZE;
  float sum;
  global[0] = count;
  global[1] = count;

  in_a = (float *) malloc (count * count * sizeof (float));
  in_b = (float *) malloc (count * count * sizeof (float));
  out = (float *) malloc (count * count * sizeof (float));

  /* Fill the vector with random float values.  */
  for (int i = 0; i < count*count; i++) {
    //in_a[i] = rand () / (float) RAND_MAX;
    //in_b[i] = rand () / (float) RAND_MAX;
    in_a[i] = (float) i;
    in_b[i] = (float) i;
    out[i] = 0.0;
  }


  if( argc > 3) {
    //printf( "using openCL on host!\n");
    err = initCPU();
  } else  {
    //printf( "using openCL on GPU!\n");
    err = initGPU();
  }
  
  if( err == CL_SUCCESS) {
    // Fill in here:
    kernel = setupKernel( KernelSource, "matmul", 4, FloatArr, count*count, in_a,
			                                         FloatArr, count*count, in_b,
													 FloatArr, count*count, out,
													 IntConst, count);

    // Fill in here:
    runKernel( kernel, 2, global, local);
  
    clock_gettime( CLOCK_PROCESS_CPUTIME_ID, &stop);

    printKernelTime();
    //printTimeElapsed( "CPU time spent");

    /* Validate our results.  */
    correct = 0;
    for (int i = 0; i < count; i++) {
      for (int j = 0; j < count; j++) {
        sum = 0.0;
        for (int k = 0; k < count; k++) {
          sum += in_a[i*count+k] * in_b[k*count+j];
        }
        if ( abs(out[i*count+j] - sum) < FLT_EPSILON)
          correct++;
        else
          printf("Diff: %f\n", out[i*count+j] - sum);
      }
    }

    /* Print a brief summary detailing the results.  */
    printf ("Computed %d/%d %2.0f%% correct values\n", correct, count*count,
            (float)(count*count)/correct*100.f);

    err = clReleaseKernel (kernel);
    err = freeDevice();

    timeDirectImplementation( count, in_a, in_b, out);
    
  }
  return 0;
}

