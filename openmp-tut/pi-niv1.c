#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#define NUMSTEP 50000000
#define NUMTHREAD 10

struct timespec start, stop;

double time_elapsed()
{
  double elapsed = (stop.tv_sec -start.tv_sec)*1000.0
                  + (double)(stop.tv_nsec -start.tv_nsec)/1000000.0;
  return elapsed;
}

int main(int argc, char** argv)
{
	int i, num_t, threads;
	double x, pi, sum[10][64]={0.0}, step = 1.0 / (double) NUMSTEP;
	char *end;
	if(argc == 1)
	{
		threads = NUMTHREAD;
	}
	else
	{
		threads = strtol(argv[1], &end, 10);
		if(*end)
		{
			printf("Input was no a number, %s\n", end);
			return EXIT_FAILURE;
		}
	}
	omp_set_num_threads(threads);

	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start);
	
	#pragma omp parallel
	{
		int i, id,
		num_threads;
		double x;
		id = omp_get_thread_num();
		num_threads = omp_get_num_threads();
		if(id == 0)
		{
			num_t = num_threads;
		}
		for(i = id; i < NUMSTEP; i = i + num_threads)
		{
			x = (i + 0.5) * step;
			sum[id][0] += 4.0 / (1.0 + x * x);
		}
	}

	for(i = 0; i < num_t; i++)
	{
		pi += sum[i][0] * step;
	}

	clock_gettime( CLOCK_PROCESS_CPUTIME_ID, &stop);
	
	printf("%d @ %f = %f\n", threads, time_elapsed(), pi);
	return EXIT_SUCCESS;
}
