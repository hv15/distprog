#!/bin/bash
# Batch run application for different times

ITER=10
BINS=( "pi-init" "pi-niv1" )

for bin in "${BINS[@]}"; do
	#[ ! -x "${bin}" ] && echo "Failed to run ${bin}." && exit 1
	echo "${bin}"
	for i in $(seq 1 ${ITER}); do
		"./${bin}" "${i}"
	done
done
