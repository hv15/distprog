// TotientRance.c - Sequential Euler Totient Function (C Version)
// _compile: gcc -Wall -O -o TotientRange TotientRange.c_
// compile: gcc -Wall -pedantic -std=gnu99 -o TotientRange TotientRange.c
// run:     ./TotientRange lower_num uppper_num

// Greg Michaelson        14/10/2003
// Patrick Maier          29/01/2010 [enforced ANSI C compliance]
// Hans-Nikolai Viessmann 16/02/2014 [For CW1 F21DP, output time]

// This program calculates the sum of the totients between a lower and an 
// upper limit using C longs. It is based on earlier work by:
// Phil Trinder, Nathan Charles, Hans-Wolfgang Loidl and Colin Runciman

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

// hcf x 0 = x
// hcf x y = hcf y (rem x y)

long hcf(long x, long y)
{
    long t;

    while (y != 0) {
        t = x % y;
        x = y;
        y = t;
    }
    return x;
}


// relprime x y = hcf x y == 1

int relprime(long x, long y)
{
    return hcf(x, y) == 1;
}


// euler n = length (filter (relprime n) [1 .. n-1])

long euler(long n)
{
    long length, i;

    length = 0;
    for (i = 1; i < n; i++)
        if (relprime(n, i))
            length++;
    return length;
}


// sumTotient lower upper = sum (map euler [lower, lower+1 .. upper])

long sumTotient(long lower, long upper)
{
    long sum, i;

    sum = 0;
    for (i = lower; i <= upper; i++)
        sum = sum + euler(i);
    return sum;
}

// Some time related functions, in order to get meaningful runtime

double elapsed(struct timespec a, struct timespec b)
{
    return ((double)(b.tv_sec - a.tv_sec) * 1000.0) + 
        ((double)(b.tv_nsec - a.tv_nsec) / 1000000.0);
}

int main(int argc, char ** argv)
{
    struct timespec start, stop;
    long lower, upper, result;

    if (argc != 3) {
        printf("not 2 arguments\n");
        return 1;
    }
    sscanf(argv[1], "%ld", &lower);
    sscanf(argv[2], "%ld", &upper);

    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start);

    result = sumTotient(lower, upper);

    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &stop);

    printf("%f %ld %ld %ld\n", elapsed(start, stop), lower, upper, result);
    return EXIT_SUCCESS;
}
