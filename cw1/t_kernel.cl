//Source file of the totient kernel
#pragma OPENCL EXTENSION cl_khr_int32_base_atomics : enable
__kernel void comp_tot(__global unsigned int * input,
                       __global unsigned int * output)
{
    unsigned int t,x,y;

    unsigned int i = get_global_id(0);
    x = input[i*2];
    y = input[i*2+1];

    while (y != 0)
    {
        t = x % y;
        x = y;
        y = t;
    }

    if(x==1) atomic_inc(&output[i]);
}
