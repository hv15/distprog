#!/bin/bash
###############################################################################
# For Coursework 1 F21DP
###############################################################################
# PROF.sh will execute all presented files with iterations in order to
# generate the profiling information for gprof. Once this is done, the
# gprof executable is given the results to generate its profile, which
# is saved into to a seperate directory.
#
# A further process is the generation of valgrind/cachegrind files.
#
# Created by Hans-Nikolai Viessmann
###############################################################################
if [ ! -n "${1}" ]; then
    echo "Missing main argument" >&2
    echo "Usage: ${0} binary [[\"options\"] num_times_each]" >&2
    exit 1
fi
APP=${1}
OPTS=${2:-"15000 30000 100000"}
THRS=${3:-"1 2 3 4 5 6 7 8"}
TIMS=${4:-3}
DIR="${PWD}/${APP}-prof"
RUN="${DIR}/${APP}.log"
ERR="${DIR}/${APP}.err"

[ -d "${DIR}" ] || mkdir ${DIR}
if [ ! -x "${APP}.gprof" ]; then
    echo "Please compile ${APP}.gprof"
    exit 100
elif [ ! -x "${APP}.cache" ]; then
    echo "Please compile ${APP}.cache"
    exit 101
fi

echo "Runtime and Profiling for ${APP} by [${OPTS}]" | tee "${RUN}"
echo "This could take a very long time, have a coffee or watch a movie..."
for i in ${OPTS}; do
    for j in ${THRS}; do
        export OMP_NUM_THREADS=${j}
        for x in $(seq 1 ${TIMS}); do 
            gprof="${DIR}/${i}.${j}.${x}-gprof"
            cache="${DIR}/${i}.${j}.${x}-cache"
            echo "Range ${i}, thread ${j}, iter ${x}:" >> "${RUN}"
            echo -n "Range ${i}, thread ${j}, iter ${x}: "
            ./"${APP}.gprof" 1 ${i} >> "${RUN}"
            echo "DONE"
            
            echo -n "GPROFing, thread ${j},  iter ${x}   "
            gprof "${APP}.gprof" > "${gprof}" 2> "${ERR}"
            echo "DONE"

    #        echo -n "Running Cachegrind, iter ${x} "
    #        valgrind --tool=cachegrind --quiet \
    #            --cachegrind-out-file="${cache}" \
    #            ./"${APP}.cache" 1 ${i} > /dev/null 2> "${ERR}"
    #        echo "DONE"
        done
    done
done
