// TotientRange.c - Sequential Euler Totient Function (C Version)
// _compile: gcc -Wall -O -o TotientRange TotientRange.c_
// compile: gcc -Wall -pedantic -std=gnu99 -o TotientRange TotientRange.c
// run:     ./TotientRange lower_num uppper_num

// Greg Michaelson        14/10/2003
// Patrick Maier          29/01/2010 [enforced ANSI C compliance]
// Hans-Nikolai Viessmann 16/02/2014 [For CW1 F21DP, output time]
// Konstantin Devyatov    19/02/2014 [For CW1 F21DP, OpenCL paralellisation]

// This program calculates the sum of the totients between a lower and an 
// upper limit using C ints. It is based on earlier work by:
// Phil Trinder, Nathan Charles, Hans-Wolfgang Loidl and Colin Runciman

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include <CL/cl.h>
#include "simple.h"
#include <math.h>

#define FILLER 0
#define MAX_KERNEL_SOURCE 10000

// hcf x 0 = x
// hcf x y = hcf y (rem x y)

int hcf(int x, int y)
{
    int t;

    while (y != 0) {
        t = x % y;
        x = y;
        y = t;
    }
    return x;
}

// euler n = length (filter (relprime n) [1 .. n-1])
// unused in current implementation

int euler(int n)
{
    int length, i;

    length = 0;
    for (i = 1; i < n; i++)
        if (hcf(n, i)==1)
            length++;
    return length;
}

// sumTotient lower upper = sum (map euler [lower, lower+1 .. upper])
// unused in current implementation

int sumTotient(int lower, int upper)
{
    int sum, i;

    sum = 0;
    for (i = lower; i <= upper; i++)
        sum = sum + euler(i);
    return sum;
}

// Some time related functions, in order to get meaningful runtime

double elapsed(struct timespec a, struct timespec b)
{
    return ((double)(b.tv_sec - a.tv_sec) * 1000.0) + 
        ((double)(b.tv_nsec - a.tv_nsec) / 1000000.0);
}

//Bodo's matrix code
unsigned int * allocVector(unsigned int n)
{
      return (unsigned int *)malloc(n*sizeof(unsigned int));
}
//end of Bodo's code

int checkArray(int * input, int size, int upper)
{
    int i; int add;
    for(i=0; i < size; i+=2)
    {
        add = input[i] + input[i+1];
        if(add != upper)
        {
            printf("add is %d \n", add);
            return 1;
        }
        //printf("input[%d] = %ld, input[%d] = %ld \n",i,input[i],i+1,input[i+1]);
    }

    return 0;
}

void initInput(unsigned int * arr, unsigned int size, unsigned int n)
{
    unsigned int i, j, g;

    g = 0;

    for(i = 1; i <= n; i++)
    {
        for(j = 0; j <= i; j++)
        {
            arr[g++] = i;
            arr[g++] = j;
            //printf("G = %d, I = %d, J = %d\n", g,i,j);
        }
    }
    printf("G = %d\n", g);
}

void printArr(unsigned int * arr, unsigned int size)
{
    unsigned int i;

    for(i = 0; i < size; i++)
        //(arr[i+1] == NULL) ? printf("%d.",arr[i]) : printf("%d,",arr[i]);
        printf("%d ",arr[i]);
}

int main(int argc, char ** argv)
{
    struct timespec start, stop;
    int lower, upper, wgs;

    if (argc != 4) {
        printf("not 3 arguments\n");
        return 1;
    }
    sscanf(argv[1], "%d", &lower);
    sscanf(argv[2], "%d", &upper);
    sscanf(argv[3], "%d", &wgs);

    FILE *fp;
    char *KernelSource;
    cl_kernel kernel;
    fp = fopen("t_kernel.cl", "r");
    if (!fp) {
        fprintf(stderr, "Error: cannot read kernel source.\n");
        exit(1);
    }

    KernelSource = (char*)malloc(MAX_KERNEL_SOURCE);
    fread( KernelSource, 1, MAX_KERNEL_SOURCE, fp);
    fclose( fp );

    unsigned int inp_num, out_num, n;
    n = upper - lower + 1;
    inp_num = ((n + 1) * (n + 1)) + (n - 1);
    out_num = inp_num / 2;
    printf("inp_num = %d\nout_num = %d\n", inp_num, out_num);

    size_t local[1];
    size_t global[1];
    local[0] = (size_t) wgs;
    global[0] = (size_t) out_num;

    unsigned int * input = allocVector(inp_num);
    initInput(input, inp_num, n);
    
    unsigned int * output = allocVector(out_num);
    unsigned int i;
    for(i = 0; i < out_num; i ++) output[i] = 0;

    initGPU();
    kernel = setupKernel( KernelSource, "comp_tot", 2,
                            IntArr, inp_num, input,
                            IntArr, out_num, output
                        );

    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start);

    runKernel(kernel, 1, global, local);

    unsigned int result = 0;
    unsigned int k;
    for(k = 0; k < out_num; k++)
        result += output[k];
    //DOMINATED!!!
    result -= 2;

    clock_gettime( CLOCK_PROCESS_CPUTIME_ID, &stop);

    clReleaseKernel(kernel);
    freeDevice();

    //printArr(output, out_num);
    //printArr(input, inp_num);
    printf("\n");

    printf("%d %f %d %d %d \n", result, elapsed(start, stop), lower, upper, wgs);
    return EXIT_SUCCESS;
}
