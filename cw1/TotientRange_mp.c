// TotientRance.c - Sequential Euler Totient Function (C Version)
// _compile: gcc -Wall -O -o TotientRange TotientRange.c_
// compile: gcc -Wall -pedantic -std=gnu99 -o TotientRange TotientRange.c
// run:     ./TotientRange lower_num uppper_num

// Greg Michaelson        14/10/2003
// Patrick Maier          29/01/2010 [enforced ANSI C compliance]
// Hans-Nikolai Viessmann 16/02/2014 [For CW1 F21DP, output time]

// This program calculates the sum of the totients between a lower and an 
// upper limit using C longs. It is based on earlier work by:
// Phil Trinder, Nathan Charles, Hans-Wolfgang Loidl and Colin Runciman

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <omp.h>

int numthread;

int hcf(long x, long y)
{
	long t;
	while (y != 0) {
		t = x % y;
		x = y;
		y = t;
	}
	return x == 1;
}

long sumTotient(long lower, long upper)
{
	long sum, i, j;
	sum = 0;
    #pragma omp parallel
    {
        numthread = omp_get_num_threads();
        #pragma omp for schedule(guided) private(i, j) reduction(+:sum)
        for (i = lower; i <= upper; i++)
        {
            for (j = 1; j < i; j++)
            {
                sum += hcf(i, j);
            }
        }
    }
	return sum;
}

int main(int argc, char ** argv)
{
	long lower, upper, result;
	double pstart, pend;

	if (argc != 3) {
		printf("not 2 arguments\n");
		return EXIT_FAILURE;
	}
	sscanf(argv[1], "%ld", &lower);
	sscanf(argv[2], "%ld", &upper);

	pstart = omp_get_wtime();

	result = sumTotient(lower, upper);

	pend = omp_get_wtime();

	printf("%f %d %ld %ld\n", (pend - pstart) * 1000, numthread, upper, result);
	return EXIT_SUCCESS;
}
