/*
   hello2.c - Hello World with MPI

   Assumes a configuration file m4, containing names of all machines.
   Default file name is mpd.hosts

   configure: mpdboot --file=m4  --totalnum=4 --verbose
   compile: mpicc -Wall -O -o hello2 hello2.c
   run:     mpirun -machinefile m4 -np 4 hello2
*/

#include <stdio.h>
#include <unistd.h>
#include <mpi.h>

int main(int argc, char ** argv)
{
  int p;                                // size
  int id;                               // rank
  char name[80];
  int z;

  MPI_Init(&argc, &argv);               // start up "virtual machine"
  MPI_Comm_size(MPI_COMM_WORLD, &p);    // get size of VM
  MPI_Comm_rank(MPI_COMM_WORLD, &id);   // get own rank in VM

  z = gethostname(name,80);
  if (z==0) { 
    printf("Hello, I am %d of %d (hostname is %s)\n", id, p, name);  // payload
  } else {
    printf("Hello, I am %d of %d (hostname UNKNOWN)\n", id, p);     // payload
  }

  MPI_Finalize();                       // shut down VM
  return 0;
}

/* 
  Use a host file like this (name m4 in the example above; mpd.hosts is default name):
bwlf17
bwlf18
bwlf19
bwlf20
*/
